//
//  PracticeAppApp.swift
//  PracticeApp
//
//  Created by Maani on 9/15/21.
//

import SwiftUI

@main
struct PracticeAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
